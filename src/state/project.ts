import {ProjectType, IProjectType} from "../models/interfaces";
import Listener = App.Listener;

export class Project {
    public title: string
    public description: string
    public people: number
    public id: string
    public type: ProjectType

    constructor(project: IProjectType) {
        this.title = project.title
        this.description = project.description
        this.people = project.people
        this.id = project.id
        this.type = project.type
    }
}

class State<T> {
    protected listeners: Listener<T>[] = []

    addListener(listener: Listener<T>) {
        this.listeners.push(listener)
    }
}


// Project State Class
// =============================================================================
class ProjectState extends State<Project> {
    private static instance: ProjectState
    private projects: Project[] = []

    private constructor() {
        super()
    }

    static getInstance() {
        return this.instance
            ? this.instance
            : new ProjectState()
    }

    runListeners() {
        this.listeners.forEach(listener => {
            listener([...this.projects])
        })
    }

    addProject(newProject: Project) {
        this.projects.push(newProject);
        this.runListeners();
    }

    moveProject(projectId: string, type: ProjectType) {
        const projectToMove = this.projects.find(project => project.id === projectId)
        if (!projectToMove || projectToMove.type === type) return
        projectToMove.type = type
        this.projects.map(project => {
            if (project.id === projectId) return projectToMove
            return project
        });
        this.runListeners();
    }

}

export const projectState = ProjectState.getInstance()
