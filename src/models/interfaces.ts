export enum ProjectType {
    Active = 'active',
    Finished = 'finished'
}

export interface ValidationRules {
    required?: boolean
    minLength?: number
    maxLength?: number
    min?: number
    max?: number
}

export interface IProjectType {
    title: string
    description: string
    people: number
    id: string
    type: ProjectType
}


export interface Draggable {
    dragStartHandler(event: DragEvent): void

    dragEndHandler(event: DragEvent): void
}

export interface DragTarget {
    dragOverHandler(event: DragEvent): void

    dropHandler(event: DragEvent): void

    dragLeaveHandler(event: DragEvent): void
}
