export default abstract class Component<T extends HTMLElement, U extends HTMLElement> {
    readonly element: U;
    private templateElement: HTMLTemplateElement;
    private containerElement: T;

    protected constructor(
        containerId: string,
        templateId: string,
        elementId: string,
        private insertType: 'afterbegin' | 'beforeend',
    ) {
        this.containerElement = document.getElementById(containerId)! as T;
        this.templateElement = document.getElementById(templateId)! as HTMLTemplateElement;
        this.element = this.cloneTemplate().firstElementChild! as U;
        if (elementId) this.element.id = elementId
        this.append();
    }

    abstract bindListeners(): void

    private cloneTemplate(): DocumentFragment {
        return document.importNode(this.templateElement.content, true);
    }

    private append(): void {
        this.containerElement.insertAdjacentElement(this.insertType, this.element);
    }
}
