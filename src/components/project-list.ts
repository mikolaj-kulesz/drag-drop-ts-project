import Cmp from "./base-project-component";
import {DragTarget, ProjectType} from "../models/interfaces";
import {Project, projectState} from "../state/project";
import * as Utils from "../utils/auto-bind";
import {ProjectItem as ProjectSingleItem} from "./project-item";

export class ProjectList extends Cmp<HTMLDivElement, HTMLElement> implements DragTarget {
    private projects: Project[] = [];

    constructor(private type: ProjectType) {
        super('app', 'project-list', `${type}-projects`, 'beforeend');
        this.renderContent();
        this.bindListeners();
    }

    get list() {
        return this.element.querySelector('ul')!
    }

    bindListeners() {
        projectState.addListener((projects: Project[]) => {
            this.projects = projects.filter(project => {
                if (this.type === 'active') return project.type === ProjectType.Active
                return project.type === ProjectType.Finished
            })
            this.renderProjects()
        })

        //drag&drop events
        this.element.addEventListener('drop', this.dropHandler)
        this.element.addEventListener('dragover', this.dragOverHandler)
        this.element.addEventListener('dragleave', this.dragLeaveHandler)
    }

    @Utils.AutoBind
    dragOverHandler(event: DragEvent): void {
        if (!event.dataTransfer || event.dataTransfer.types[0] !== 'text/plain') return
        event.preventDefault()
        this.list.classList.add('droppable')
    }

    @Utils.AutoBind
    dropHandler(event: DragEvent): void {
        const projectId = event.dataTransfer!.getData('text/plain')
        projectState.moveProject(projectId, this.type)
    }

    @Utils.AutoBind
    dragLeaveHandler(event: DragEvent): void {
        this.list.classList.remove('droppable')
    }

    private renderContent(): void {
        this.element.querySelector('ul')!.id = `${this.type}-projects-list`;
        this.element.querySelector('h2')!.textContent = `${this.type.toUpperCase()} PROJECTS`
    }

    private renderProjects(): void {
        const listElement = document.getElementById(`${this.type}-projects-list`)! as HTMLUListElement;
        listElement.innerHTML = ''
        this.projects.forEach(project => {
            new ProjectSingleItem(project);
        })
    }
}

