import Cmp from "./base-project-component";
import {ValidationRules, ProjectType} from "../models/interfaces";
import {AutoBind} from "../utils/auto-bind";
import {Project, projectState} from "../state/project";

export class ProjectInput extends Cmp<HTMLDivElement, HTMLFormElement> {
    private formInputElements: Map<string, HTMLInputElement> = new Map();
    private formInputValues: Map<string, string> = new Map();
    private formInputValidationStatuses: Map<string, boolean> = new Map();
    private formInputsValidationsRules: Map<string, ValidationRules> = new Map();
    private formInputElementsIds: string[] = ['title', 'description', 'people'];

    constructor() {
        super('app', 'project-input', 'user-input', 'afterbegin');
        this.collectFormInputElements();
        this.setValidationRules();
        this.bindListeners();
    }

    bindListeners(): void {
        this.element.addEventListener('submit', this.submitHandler);
    }

    private collectFormInputElements() {
        this.formInputElementsIds.forEach(id => {
            return this.formInputElements.set(id, this.element.querySelector(`#${id}`)! as HTMLInputElement);
        })
    }

    private setValidationRules() {
        this.formInputElementsIds.forEach(id => {
            switch (id) {
                case 'title':
                    this.formInputsValidationsRules.set(id, {
                        required: true,
                        minLength: 3,
                        maxLength: 20,
                    })
                    break;
                case 'description':
                    this.formInputsValidationsRules.set(id, {
                        required: true,
                        minLength: 5,
                    })
                    break;
                case 'people':
                    this.formInputsValidationsRules.set(id, {
                        required: true,
                        min: 0,
                        max: 10,
                    })
                    break;
            }
        })
    }

    @AutoBind
    private submitHandler(e: Event) {
        e.preventDefault();
        this.formInputElements.forEach((element, key) => {
            this.formInputValues.set(key, element.value)
        });

        this.validateFormItems()
        if (this.isFormValid()) {
            // alert('Form is valid!');
            this.clearInputFields()
            projectState.addProject(new Project({
                title: this.formInputValues.get('title')!,
                description: this.formInputValues.get('description')!,
                people: +this.formInputValues.get('people')!,
                id: Math.random().toString(),
                type: ProjectType.Active,
            }))
            return
        }
        alert('Form is invalid!');
    }

    private clearInputFields(): void {
        this.formInputElements.forEach((element, key) => {
            element.value = ''
        });
    }

    private isRequired(value: any): boolean {
        return !!value
    }

    private isMinLength(value: any, min: number | undefined): boolean {
        return min ? value.toString().trim().length >= min : true

    }

    private isMaxLength(value: any, max: number | undefined): boolean {
        return max ? value.toString().trim().length <= max : true
    }

    private isMin(value: any, min: number | undefined): boolean {
        return min ? parseInt(value) >= min : true;
    }

    private isMax(value: any, max: number | undefined): boolean {
        return max ? parseInt(value) <= max : true;
    }

    private isFormValid() {
        const statuses: boolean[] = [];
        this.formInputValidationStatuses.forEach(value => {
            statuses.push(value)
        })
        return !statuses.includes(false);
    }

    private validateFormItems(): void {
        this.formInputValues.forEach((value, key) => {
            const validationRules = this.formInputsValidationsRules.get(key)
            let results = [true]
            if (validationRules) {
                results = Object.keys(validationRules).map(rule => {
                    switch (rule) {
                        case 'required':
                            return this.isRequired(value)
                        case 'minLength':
                            return this.isMinLength(value, validationRules[rule])
                        case 'maxLength':
                            return this.isMaxLength(value, validationRules[rule])
                        case 'min':
                            return this.isMin(value, validationRules[rule])
                        case 'max':
                            return this.isMax(value, validationRules[rule])
                        default:
                            return true
                    }
                })
            }
            const isValid = !results.includes(false);
            this.formInputValidationStatuses.set(key, isValid)
        })
    }
}
