import {ProjectList} from "./components/project-list";
import {ProjectInput} from "./components/project-input";
import {ProjectType} from "./models/interfaces";
import {DemoProduct} from "./components/demo-product";
import _ from "lodash";
import "reflect-metadata";
import {plainToClass} from "class-transformer";
import {validate} from "class-validator";


// document.addEventListener("DOMContentLoaded", () => {
const projectInput = new ProjectInput();
const activeList = new ProjectList(ProjectType.Active)
const finishedList = new ProjectList(ProjectType.Finished)

declare var GLOBAL: string

console.log(_.shuffle([1, 2, 3]))
console.log('GLOBAL:', GLOBAL)

const demoProduct = new DemoProduct('CAR', 10000);
console.log(demoProduct.getInfo());

const products = [
    {
        title: 'Book',
        price: 12.45,
    },
    {
        title: 'Pen',
        price: 2.25,
    }
]

const classProducts = plainToClass(DemoProduct, products);
classProducts.forEach(p => console.log(p.getInfo()));

const newProd = new DemoProduct('', -20)
validate(newProd).then(errors => {
    if(errors.length > 0){
        console.log('VALIDATION ERRORS')
        console.log(errors)
        return
    }
    console.log(newProd.getInfo())
})
// });
